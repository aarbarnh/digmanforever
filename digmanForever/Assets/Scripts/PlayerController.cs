﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Transform tf; //reference to player transform
    private SpriteRenderer sr;
    private Rigidbody2D rb; //reference to player rigidbody2D
    private Animator anim;
    private bool inAir;

    //audio clips
    public AudioClip checkClip;
    public AudioClip runeClip;
    public AudioClip medalClip;
    public AudioClip deathClip;

    //player variables
    public Vector3 respondPt; //if I need a homePos reset, maybe update with checkpoints
    public float moveX;
    public float moveSpeed = 5.0f;
    public float jumpSpeed = 50.0f;
    public int distanceToFeet = 1;
    public int maxJumps = 1;
    public int jumpsUsed = 0;
    public bool faceRight = true; //control facing

    //background variable
    public Transform bTf;
    public Vector3 bHomePos;

    // Start is called before the first frame update
    void Start()
    {
        //getting components
        rb = GetComponent<Rigidbody2D>();
        tf = GetComponent<Transform>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        respondPt = tf.position; //set home pos
        bHomePos = bTf.position; //get the starting position of background image, reset each death so it hopefully doesnt scroll out
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        moveX = Input.GetAxis("Horizontal") * moveSpeed; //getting horizontal movement, left right
        rb.velocity = new Vector2(moveX, rb.velocity.y); //apply to velocity to move

        if (rb.velocity.x > 0.1f && !inAir)
        {
            anim.Play("WalkCycle");
            sr.flipX = false;
        }
        else if (rb.velocity.x < -0.1f && !inAir)
        {
            anim.Play("WalkCycle");
            sr.flipX = true;
        }
        else if (!inAir)
        {
            anim.Play("IdleState");
        }

        RaycastHit2D ground = Physics2D.Raycast(tf.position, Vector2.down, distanceToFeet);
        if (ground.collider != null)
        {
            inAir = false;
            jumpsUsed = 0;
        }
        else
        {
            inAir = true;
        }

        //jumping
        if (Input.GetButtonDown("Jump") && jumpsUsed < maxJumps) 
        {
            rb.velocity += Vector2.up * jumpSpeed;
            jumpsUsed++;
        }
        if (inAir)
        {
            anim.Play("Jump");
        }
    }

    void OnTriggerEnter2D(Collider2D other) //all trigger detection, pick ups and tilemap triggers 
    {
        if (other.gameObject.CompareTag("Rune")) //rune pickup 
        {
            Destroy(other.gameObject); //destroy rune 
            GameManager.game.levelOneRunes++;
            AudioSource.PlayClipAtPoint(runeClip, tf.position); //play clip at point 
            if (GameManager.game.levelOneRunes == 1) //manage UI
            {
                GameManager.game.runeE.gameObject.SetActive(true);
            }
            else if (GameManager.game.levelOneRunes == 2)
            {
                GameManager.game.runeN.gameObject.SetActive(true);
            }
            else if (GameManager.game.levelOneRunes == 3)
            {
                GameManager.game.runeD.gameObject.SetActive(true);
            }
        }
        else if (other.gameObject.CompareTag("Hazard")) //hit tilemap hazard 
        {
            LoseLife(); //lose life call 
        }
        else if (other.gameObject.CompareTag("CheckPt")) //checkpt tilemap hit
        {
            respondPt = tf.position; //make respond point the checkpoint position (player at checkpoint)
            AudioSource.PlayClipAtPoint(checkClip, tf.position);
        }
        else if (other.gameObject.CompareTag("Medals")) //medals hit 
        {
            Destroy(other.gameObject); //destroy 
            GameManager.game.levelOneMedals++;
            AudioSource.PlayClipAtPoint(medalClip, tf.position); //play clip 
            if (GameManager.game.levelOneMedals == 1) //UI manage
            {
                GameManager.game.medalOne.gameObject.SetActive(true);
            }
            else if (GameManager.game.levelOneMedals == 2)
            {
                GameManager.game.medalTwo.gameObject.SetActive(true);
            }
            else if (GameManager.game.levelOneMedals == 3)
            {
                GameManager.game.medalThree.gameObject.SetActive(true);
            }
        }
    }

    void LoseLife() //lose life function, decrement lives, play death sound, respond at last checkpoint and change UI
    {
        GameManager.game.playerLives--;
        AudioSource.PlayClipAtPoint(deathClip, tf.position); //play clip 
        tf.position = respondPt; //reset position to last checkpoint 
        bTf.position = bHomePos; //reset background 
        if (GameManager.game.playerLives == 2) //manage UI
        {
            GameManager.game.lifeOne.gameObject.SetActive(false);
        }
        else if (GameManager.game.playerLives == 1)
        {
            GameManager.game.lifeTwo.gameObject.SetActive(false);
        }
        else
        {
            GameManager.game.lifeThree.gameObject.SetActive(false);
            GameManager.game.SetScene(3); //if last life, set to lose scene
        }
    }

}
