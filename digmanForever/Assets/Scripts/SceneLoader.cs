﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadScene(string scene) //load scene for button (main scene start button)
    {
        SceneManager.LoadScene(scene);
    }

    public void QuitGame() //quit function for button
    {
        Application.Quit();
    }
}
