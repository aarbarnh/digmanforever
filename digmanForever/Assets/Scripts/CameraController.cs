﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //camera variables
    private Transform tf;
    private Vector3 offset; 
    public Transform pTf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        offset = tf.position - pTf.position; //get offset
    }

    // Update is called once per frame
    void LateUpdate()
    {
        tf.position = pTf.position + offset; //update camera position, same as players but offset 
    }
}
