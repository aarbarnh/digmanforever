﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager game; //singleton

    //game variables 
    public int levelOneRunes = 0;
    public int levelOneMedals = 0;
    public Image runeE, runeN, runeD; //rune images
    public Image medalOne, medalTwo, medalThree; //medal images 

    //player variables 
    public int playerLives = 3;
    public Image lifeOne, lifeTwo, lifeThree;

    //enum for scene change
    public enum SceneSelect
    {
        StartMenu,
        GameOne,
        Win,
        Lose
    }
    public SceneSelect sSelect;

    void Awake()
    {
        if (game == null) //singleton
        {
            game = this;
        }
        else
        {
            Destroy(gameObject); //make sure only one
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) //quick quit
        {
            Application.Quit();
        }
    }

    public void SetScene(int state) //sets the state to what scene we are on
    {
        sSelect = (SceneSelect)state;
        ActivateScene();
    }

    void ActivateScene() //activates the set scene
    {
        switch(sSelect)
        {
            case SceneSelect.StartMenu:
                SceneManager.LoadScene("Main");
                break;
            case SceneSelect.GameOne:
                SceneManager.LoadScene("GameOne");
                break;
            case SceneSelect.Win:
                SceneManager.LoadScene("Win");
                break;
            case SceneSelect.Lose:
                SceneManager.LoadScene("Lose");
                break;
        }
    }
}
