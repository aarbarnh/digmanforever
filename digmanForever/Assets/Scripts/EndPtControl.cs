﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPtControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Player") && GameManager.game.levelOneRunes == 3) //if player hits endpt and has all three runes 
        {
            GameManager.game.SetScene(2); //set to win scene
        }
    }
}
